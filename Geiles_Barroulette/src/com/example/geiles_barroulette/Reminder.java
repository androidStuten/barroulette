package com.example.geiles_barroulette;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;


public class Reminder extends Service implements
		OnBarTimerStatusChangedListener {

	private static final int myNotificationID = 12345;
	private OnBarTimerStatusChangedListener onBarTimerStatusChangedListener;
	private IBinder iBinder;
	private int timePerBar = 3000;
	private Timer timer;
	private static final String timerStarted = "timerStarted";

	@Override
	public void onCreate() {

		iBinder = new LocalBinder();
	}

	
// stoppt den timer
	
	public void stopTimer() {
		if (timer != null) {
			timer.cancel();
		}
	}
	
	//wird automatish aufgerufen, sticky/not_sticky

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		timer = new Timer(this, timePerBar);
		

		if (timer.getTimerStatus() != 2) {
			timer.start();
			return START_STICKY;
		} else {
			timer.cancel();
			return START_NOT_STICKY;
		}
	}

	public void setOnBarTimerStatusChangedListener(
			OnBarTimerStatusChangedListener onBarTimerStatusChangedListener) {
		this.onBarTimerStatusChangedListener = onBarTimerStatusChangedListener;
	}
	
	// sendet notification

	private void sendNotification() {

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.reminder)
				.setContentTitle(getString(R.string.dialogTitle))
				.setContentText(getString(R.string.notificationText));
		Uri uri = Uri.parse("android.resource://" + getPackageName() + "/"
				+ R.raw.prosit);
		mBuilder.setSound(uri);

		// Creates an explicit intent for an Activity in your app
		Logic.setNewTour(false);
		Logic.setTimerStarted(false);
		
		Intent resultIntent = new Intent(this, BarTourActivity.class);
		resultIntent.putExtra(timerStarted, true);

		// The stack builder object will contain an artificial back stack for
		// the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(BarTourActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(resultPendingIntent);

		mBuilder.setAutoCancel(true);
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		mNotificationManager.notify(myNotificationID, mBuilder.build());

		Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
		vibrator.vibrate(2000);

	}

	class LocalBinder extends Binder {

		Reminder getBinder() {
			return Reminder.this;
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return iBinder;
	}

	public void onBarTimerFinished() {
		Logic.setTimerStarted(true);

		if (onBarTimerStatusChangedListener != null && Logic.getIsVisible())
			onBarTimerStatusChangedListener.onBarTimerFinished();
		if (!Logic.getIsVisible())
			sendNotification();

	}

}
