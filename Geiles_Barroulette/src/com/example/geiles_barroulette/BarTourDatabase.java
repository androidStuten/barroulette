package com.example.geiles_barroulette;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BarTourDatabase {

	// Parameters for the database
	private static final String DATABASE_NAME = "barTour.db";
	private static final int DATABASE_VERSION = 1;

	// Name of database table
	private static final String DATABASE_TABLE = "BarItems";
	// Set the names for database columns in table (we only have two columns)
	public static final String KEY_ID = "_id";
	public static final String KEY_NAME = "name";
	public static final String KEY_TELNUM = "phonenumber";
	public static final String KEY_ADRESS = "adress";
	public static final String KEY_IS_CLUB = "isclub";
	public static final String KEY_HOURS = "openinghours";
	public static final String KEY_IS_PEDRINK = "ispredrink";

	// Stor index of task-column
	public static final int COLUMN_NAME_INDEX = 1;
	public static final int COLUMN_TELNUM_INDEX = 2;
	public static final int COLUMN_ADRESS_INDEX = 3;
	public static final int COLUMN_IS_CLUB_INDEX = 4;
	public static final int COLUMN_HOURS_INDEX = 5;
	public static final int COLUMN_IS_PREDRINK_INDEX = 6;

	public static final String TAG = "Database";

	private BarTourDBOpenHelper dbHelper;
	private SQLiteDatabase db;

	public BarTourDatabase(Context context) {
		dbHelper = new BarTourDBOpenHelper(context, DATABASE_NAME, null,
				DATABASE_VERSION);

	}
	
	private class BarTourDBOpenHelper extends SQLiteOpenHelper {
		private static final String DATABASE_CREATE = "create table "
				+ DATABASE_TABLE + " (" 
				+ KEY_ID + " integer primary key autoincrement, "
				+ KEY_NAME + " text not null, "
				+ KEY_TELNUM + " text, "
				+ KEY_ADRESS + " text, "
				+ KEY_IS_CLUB + " text, "
				+ KEY_HOURS + " text, "
				+ KEY_IS_PEDRINK + " text);";

		public BarTourDBOpenHelper(Context c, String dbname,
				SQLiteDatabase.CursorFactory factory, int version) {
			super(c, dbname, factory, version);
			Log.d(TAG, DATABASE_CREATE);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}


		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		}
	}
	
	public void open() throws SQLException {
		try {
			Log.d(TAG, "try: getWritableDatabase()");
			db = dbHelper.getWritableDatabase();
		} catch (SQLException e) {
			Log.d(TAG, "catch: getReadableDatabase()");
			db = dbHelper.getReadableDatabase();
		}
		Log.d(TAG, "...succeeded");
	}
	
	public void close() {
		db.close();
	}

	
	public long insertBarItem(BarItem item) {
		ContentValues newHighscoreValues = new ContentValues();
		
		newHighscoreValues.put(KEY_NAME, item.getName());
		newHighscoreValues.put(KEY_TELNUM, item.getPhoneNumber());
		newHighscoreValues.put(KEY_ADRESS, item.getAdress()[0] + ", " + item.getAdress()[1]);
		newHighscoreValues.put(KEY_IS_CLUB, String.valueOf(item.getIsClub()));
		newHighscoreValues.put(KEY_HOURS, item.getOpeninghours());
		newHighscoreValues.put(KEY_IS_PEDRINK, String.valueOf(item.getIsPreDrink()));

		return db.insert(DATABASE_TABLE, null, newHighscoreValues);
	}
	
	public void removeBarItem(BarItem item) {
		String whereClause = KEY_NAME + " = '" + item.getName() + /*"' AND " + 
				KEY_TELNUM + " = '" + item.getPhoneNumber() + "' AND " +
				KEY_NAME + " = '" + item.getAdress()[0] + ", " + item.getAdress()[1] +*/ "'";

		db.delete(DATABASE_TABLE, whereClause, null);
	}
	
	public Cursor getAllBarItems() {
		return db.query(DATABASE_TABLE, new String[] {KEY_ID, KEY_NAME, KEY_TELNUM, KEY_ADRESS, KEY_IS_CLUB, KEY_HOURS, KEY_IS_PEDRINK},
				null, null, null, null, null);
	}
}
