package com.example.geiles_barroulette;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class DrunkenessAnalyzerActivity extends Activity {

	private static final String SOBER = "SUPER, Du bist nuechtern und kannst getrost weiter trinken!";
	private static final String LITTLE_DRUNK = "Noch bist du ganz gut zu Fuss, trink ruhig weiter!";
	private static final String DRUNK = "Langsam wirds kritisch, du solltest ein wenig vom Gas gehen!";
	private static final String ADVANCED_DRUNK = "Ohje, spaetestens jetzt solltest du wirklich aufhoeren!";
	private static final String TOTALLY_DRUNK = "Falls du das noch lesen kannst: Sieh zu wie du Heim kommst oder ruf nen Krankenwagen";
	
	private static final String TAG = "DunkenessAnalyzer";
	
	private ImageButton startStopButton, restartButton;
	private TextView resultView;
	private ImageView startMaskotzchen, stopMaskotzchen, instructionView,
			vomittingMaskotzchen;
	private boolean clicked = false;
	private float maxX = 0;
	private float maxY = (float) -9.81;
	
	private SensorManager sensorManager;
	private Sensor sensor;
	private SensorEventListener sensorEventListener;
	private Typeface font;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_drunkeness_analyzer);
		Log.d(TAG, "onCreate");
		setupUiElements();
		initStartStopButton();
	}

	// setzt onClickListener auf die Button

	private void initStartStopButton() {
		startStopButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (clicked == false) {
					clicked = true;
					setImageViewsToAnalyzerStartedMode();
					initSensor();
								
				} else {
					setImageViewsToAnalyerStopedMode();
					determineDrunkeness();
					
				}
			}
		});

		restartButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				clicked = true;
				resetImageViewsAndSensor();
				initSensor();
			}
		});

	}
	
	private void initSensor(){
		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		int sensorType = Sensor.TYPE_ACCELEROMETER;
		sensorManager.registerListener(sensorEventListener,
				sensorManager.getDefaultSensor(sensorType),
				SensorManager.SENSOR_DELAY_UI);
	
	//this method gets the values of the two sensors
	
	sensorEventListener = new SensorEventListener() {
    	@Override
    	public void onSensorChanged(SensorEvent sensorEvent) {
    		if (sensorEvent.sensor.getType() == sensor.TYPE_ACCELEROMETER) {
    			float xSensor = sensorEvent.values[0];
    			float ySensor = sensorEvent.values[1];
    		
    			if (xSensor > maxX) {
    				maxX = xSensor;
    			}
    			
    			if (ySensor > maxY) {
    				maxY = ySensor;
    			}
    		}
    		
    	}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method  
			
			}
		};
	}
    
    
    protected void onResume() {
        super.onResume();
//        sensorManager.registerListener(sensorEventListener, sensor, SensorManager.SENSOR_DELAY_UI);
    }
    
    protected void onPause() {
        super.onPause();
      //  sensorManager.unregisterListener(sensorEventListener);
    }
    
    //determines Drunkeness in Values of 10 in each  direction
    
    private void determineDrunkeness(){
    	if ((maxX > -2 && maxX < 2)||(maxY > -2 && maxY < 2)){
    		resultView.setText(SOBER);
    	}else if((maxX < -2 && maxX > 4)||(maxX > 2 && maxX < 4)||(maxY < -2 && maxY > -4)||(maxY > 2 && maxY < 4)){
    		resultView.setText(LITTLE_DRUNK);
    	}else if((maxX < -4 && maxX > -7)||(maxX > 4 && maxX < 7)||(maxY < -4 && maxY > -7)||(maxY > 4 && maxY < 7)){
    		resultView.setText(DRUNK);
    	}else if((maxX < -7 && maxX > -11)||(maxX > 7 && maxX < 11)||(maxY < -7 && maxY > -11)||(maxY > 7 && maxY < 11)){
    		resultView.setText(ADVANCED_DRUNK);
    	}else if((maxX < -11 || maxX > 11)||(maxY < -11 || maxY > 11)){
    		resultView.setText(TOTALLY_DRUNK);
    	}
    }

	private void resetImageViewsAndSensor() {
		startStopButton.setBackgroundResource(R.drawable.stopbutton);
		startStopButton.setVisibility(View.VISIBLE);
		startStopButton.setVisibility(View.VISIBLE);
		startStopButton.setVisibility(View.VISIBLE);
		instructionView.setVisibility(View.INVISIBLE);
		startMaskotzchen.setVisibility(View.INVISIBLE);
		stopMaskotzchen.setVisibility(View.VISIBLE);
		vomittingMaskotzchen.setVisibility(View.INVISIBLE);
		resultView.setText("");
		maxX = 0;
		maxY = (float) -9.81;
	}

	private void setImageViewsToAnalyerStopedMode() {
		stopMaskotzchen.setVisibility(View.INVISIBLE);
		vomittingMaskotzchen.setVisibility(View.VISIBLE);
		startStopButton.setVisibility(View.INVISIBLE);
		restartButton.setVisibility(View.VISIBLE);
	}

	private void setImageViewsToAnalyzerStartedMode() {
		startStopButton.setBackgroundResource(R.drawable.stopbutton);
		instructionView.setVisibility(View.INVISIBLE);
		startMaskotzchen.setVisibility(View.INVISIBLE);
		stopMaskotzchen.setVisibility(View.VISIBLE);
	}

	private void setupUiElements() {
		startStopButton = (ImageButton) findViewById(R.id.start_button);
		restartButton = (ImageButton) findViewById(R.id.nochmal_button);
		resultView = (TextView) findViewById(R.id.resultView);
		startMaskotzchen = (ImageView) findViewById(R.id.start_maskotzchen);
		stopMaskotzchen = (ImageView) findViewById(R.id.stop_maskotzchen);
		instructionView = (ImageView) findViewById(R.id.anleitung);
		vomittingMaskotzchen = (ImageView) findViewById(R.id.vomitting_maskotzchen);
		
		font = Typeface.createFromAsset(getAssets(),
				"fonts/alba.ttf");
		resultView.setTypeface(font);
		resultView.setTextColor(Color.YELLOW);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
