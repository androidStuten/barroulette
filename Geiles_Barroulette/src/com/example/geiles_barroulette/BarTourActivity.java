package com.example.geiles_barroulette;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ShareActionProvider;
import android.widget.Toast;

import com.mobeta.android.dslv.DragSortController;
import com.mobeta.android.dslv.DragSortListView;

public class BarTourActivity extends Activity implements
		OnBarTimerStatusChangedListener {

	private static final int SHOW_ON_MAP = 0;
	private static final int SHOW_ROUTE = 1;
	private static final int KEBAP = 2;
	private static final int BEER_TO_GO = 3;
	private static final int TAKE_ME_HOME = 4;
	
	private static final int CASE_MARKERS = 0;
	private static final int CASE_ROUTE = 1;
	private static final int CASE_CALL = 2;

	private static final int TAKE_PICTURE = 0;

	private static final String TAG = "BarTourActivity";
	
	private static final String DEFAULT_NAME = "Vorglühen";
	private static final String[] barOptions = { "Zeige auf Karte",
			"Route anzeigen", "Anrufen" };	
	
	private DragSortListView listView;
	private BarTourDatabase database;
	private BarTourAdapter adapter;
	private ArrayList<BarItem> list;
	private Cursor dbCursor;
	private ShareActionProvider mShareActionProvider;
	private Reminder reminder;
	private ServiceConnection serviceConnection;
	private Logic logic;
	private ImageButton kebapButton, takeMeHomeButton, beerButton,
			drunkAnalyzerButton;
	
	private int selectedListItem;
	private int menuSettings;
	private boolean timerStarted = false;
	private int menuHelp;

	
	
	
	private DragSortListView.DropListener onDrop = new DragSortListView.DropListener()
	{
	    @Override
	    public void drop(int from, int to)
	    {
	        if (from != to)
	        {
	            BarItem item = adapter.getItem(from);
	            adapter.remove(item);
	            adapter.insert(item, to);
	        }
	    }
	};

	private DragSortListView.RemoveListener onRemove = new DragSortListView.RemoveListener()
	{
	    @Override
	    public void remove(int which)
	    {
	        adapter.remove(adapter.getItem(which));
	    }
	};

	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bar_tour);
		
		setupUIAndList();
		fillList();

		setClickListener();
		createShareIntent();
		initServiceConnection();
		
		Log.d(TAG, "onCreate: done");
		
	}

	private void initServiceConnection() {

		serviceConnection = new ServiceConnection() {

			@Override
			public void onServiceDisconnected(ComponentName name) {
				System.out.println("Service disconnected");
			}

			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				System.out.println("Service connected");

				reminder = ((Reminder.LocalBinder) service).getBinder();
				if (reminder != null)
					reminder.setOnBarTimerStatusChangedListener(BarTourActivity.this);
			}
		};
	}

	private void fillList() {

		if (Logic.isNewTour()) {
			fillListNewTour();
		} else {
			fillListOldTour();
		}
	}

	private void fillListOldTour() {
		connectToDatabase();
		updateList();
	}

	private void fillListNewTour() {

		int[] barClubCount = determineBarClubCount();
		int bars = barClubCount[0];
		int clubs = barClubCount[1];
		int preDrinks = Logic.getPreDrinks();
		
		if (preDrinks != 0) {		
			BarItem preDrinkItem = getPreDrinkItem(preDrinks);		
			list.add(preDrinkItem);
		}

		for (int i = 0; i < bars; i++) {

			BarItem currentItem = logic.getRandomBar();
			while (list.contains(currentItem)) {
				currentItem = logic.getRandomBar();
			}
			list.add(currentItem);
			Log.d(TAG, "Bar: " + currentItem.getName());
		}

		for (int i = 0; i < clubs; i++) {

			BarItem currentItem = logic.getRandomClub();
			while (list.contains(currentItem)) {
				currentItem = logic.getRandomClub();
			}
			Log.d(TAG, "Club: " + currentItem.getName());
			list.add(currentItem);
		}
		adapter.notifyDataSetChanged();
		Log.d(TAG, "fillList done");

	}

	private BarItem getPreDrinkItem(int preDrinks) {
		BarItem preDrinkItem;
		
		switch(preDrinks){
		
		case Logic.PRE_DRINKS_PRIVATE_AT_HOME:
			preDrinkItem = new BarItem(Logic.homeName);
			String[] adress = Logic.homeAdress.split(", ");
			preDrinkItem.setAdress(adress);
			preDrinkItem.setPreDrink(true);
			return preDrinkItem;
			
		case Logic.PRE_DRINKS_PUBLIC:
			preDrinkItem = logic.getRandomPreDrinkPlace();
			preDrinkItem.setPreDrink(true);
			return preDrinkItem;
			
		case Logic.PRE_DRINKS_PRIVATE_ELSEWHERE:
			preDrinkItem = new BarItem(
					Logic.elsewhereName);
			if (Logic.elsewhereAdress.length() > 1) {
				String[] adr = Logic.elsewhereAdress.split(", ");
				preDrinkItem.setAdress(adr);
			}
			preDrinkItem.setPreDrink(true);
			return preDrinkItem;
			
		default:			
			preDrinkItem = new BarItem(DEFAULT_NAME);
			preDrinkItem.setPreDrink(true);
			return preDrinkItem;
		}
	}

	private int[] determineBarClubCount() {
		int level = Logic.getLevel();
		int[] toReturn = new int[2]; // [0]=bars, [1]=clubs
		boolean isBars = Logic.getBars();
		boolean isClubs = Logic.getClubs();

		if (!isClubs && isBars) {
			toReturn[0] = level;
		}
		if (!isBars && isClubs) {
			toReturn[1] = level;
		}
		if (isClubs && isBars) {
			if (level <= 4) {
				toReturn[0] = level - 1;
				toReturn[1] = 1;
			} else if (level <= 6) {
				toReturn[0] = level - 2;
				toReturn[1] = 2;
			} else if (level <= 10) {
				toReturn[0] = level - 3;
				toReturn[1] = 3;
			}
		}
		Log.d(TAG, "level = " + level + ", davon bars: " + toReturn[0]
				+ ", clubs: " + toReturn[1]);
		return toReturn;
	}

	private void connectToDatabase() {
		database = new BarTourDatabase(this);
		database.open();

		Log.d(TAG, "database.open");

	}

	private void syncDatabaseWithList() {

		for (int i = 0; i < list.size(); i++) {
			database.insertBarItem(list.get(i));
		}
	}

	private void updateList() {

		dbCursor = database.getAllBarItems();
		list.clear();

		if (dbCursor.moveToFirst()) {
			do {

				String barName = dbCursor
						.getString(BarTourDatabase.COLUMN_NAME_INDEX);
				String phoneNum = dbCursor
						.getString(BarTourDatabase.COLUMN_TELNUM_INDEX);
				String adressUnformat = dbCursor
						.getString(BarTourDatabase.COLUMN_ADRESS_INDEX);
				String[] adress = adressUnformat.split(", ");
				String isClub = dbCursor
						.getString(BarTourDatabase.COLUMN_IS_CLUB_INDEX);
				String openingHours = dbCursor
						.getString(BarTourDatabase.COLUMN_HOURS_INDEX);
				String isPreDrink = dbCursor
						.getString(BarTourDatabase.COLUMN_IS_PREDRINK_INDEX);

				BarItem currentItem = new BarItem(barName);
				currentItem.setPhoneNumber(phoneNum);
				currentItem.setAdress(adress);
				currentItem.setIsClub(Boolean.parseBoolean(isClub));
				currentItem.setOpeninghours(openingHours);
				currentItem.setPreDrink(Boolean.parseBoolean(isPreDrink));
				list.add(currentItem);
			}

			while (dbCursor.moveToNext());
		}
		
		adapter.notifyDataSetChanged();

	}


	private void setClickListener() {

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				Log.d(TAG, "onItemClick: " + String.valueOf(arg2));
				selectedListItem = arg2;
				DialogFragment dialog = new BarSettingDialog();
				dialog.show(getFragmentManager(), "bar setting");

			}
		});

		kebapButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Logic.distinguish = KEBAP;
				Intent k = new Intent(BarTourActivity.this, MapsActivity.class);
				startActivity(k);
			}
		});

		takeMeHomeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Logic.distinguish = TAKE_ME_HOME;
				
				if (Logic.homeAdress == null) {
					Logic.setTakeMeHome(true);
					Intent t = new Intent(BarTourActivity.this,
							SettingsActivity.class)
							.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(t);

				} else {
					Intent t = new Intent(BarTourActivity.this,
							MapsActivity.class);
					startActivity(t);
				}
			}
		});

		beerButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Logic.distinguish = BEER_TO_GO;
				Intent b = new Intent(BarTourActivity.this, MapsActivity.class);
				startActivity(b);
			}
		});

		drunkAnalyzerButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent d = new Intent(BarTourActivity.this,
						DrunkenessAnalyzerActivity.class);
				startActivity(d);
			}
		});
	}

	@Override
	protected void onDestroy() {

		connectToDatabase();
		eraseDatabase();
		syncDatabaseWithList();
		database.close();

		super.onDestroy();

	}

	private void eraseDatabase() {

		dbCursor = database.getAllBarItems();
		if (dbCursor.moveToFirst()) {
			do {

				String barName = dbCursor
						.getString(BarTourDatabase.COLUMN_NAME_INDEX);
				String phoneNum = dbCursor
						.getString(BarTourDatabase.COLUMN_TELNUM_INDEX);
				// String adressUnformat = dbCursor
				// .getString(BarTourDatabase.COLUMN_ADRESS_INDEX);
				// String[] adress = adressUnformat.split(", ");

				BarItem currentItem = new BarItem(barName);
				currentItem.setPhoneNumber(phoneNum);
				// currentItem.setAdress(adress);
				database.removeBarItem(currentItem);
			} while (dbCursor.moveToNext());
		}
	}

	private void setupUIAndList() {

		kebapButton = (ImageButton) findViewById(R.id.kebapButton);
		takeMeHomeButton = (ImageButton) findViewById(R.id.takeMeHomeButton);
		beerButton = (ImageButton) findViewById(R.id.beerButton);
		drunkAnalyzerButton = (ImageButton) findViewById(R.id.drunkenessAnalyzerButtonBarTour);
		listView = (DragSortListView) findViewById(R.id.listViewBars);

		DragSortController controller = new DragSortController(listView);
	    controller.setDragHandleId(R.id.drag);
	    controller.setRemoveEnabled(true);
	    controller.setSortEnabled(true);
	    controller.setDragInitMode(1);
	    controller.setRemoveMode(DragSortController.FLING_REMOVE);		
		
	    list = new ArrayList<BarItem>();
	    adapter = new BarTourAdapter(this, list);
		adapter.notifyDataSetChanged();
			
		listView.setAdapter(adapter);
		listView.setDropListener(onDrop);
	    listView.setRemoveListener(onRemove);
		listView.setFloatViewManager(controller);
	    listView.setOnTouchListener(controller);
	    listView.setDragEnabled(true);
	    
		logic = new Logic(getApplicationContext());
		
		Log.d(TAG, "setupList: done");
	}

	private void createShareIntent() {
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.setType("image/*");
		setShareIntent(Intent.createChooser(sendIntent,
				getResources().getText(R.string.action_settings)));

	}

	// Call to update the share intent
	private void setShareIntent(Intent shareIntent) {
		if (mShareActionProvider != null) {
			mShareActionProvider.setShareIntent(shareIntent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Locate MenuItem with ShareActionProvider

		menu.add(0, menuSettings, 0, R.string.settings).setShortcut('1', 'x')
				.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem arg0) {
						Intent i = new Intent(BarTourActivity.this,
								SettingsActivity.class)
								.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(i);
						return false;
					}

				});

		menu.add(0, menuHelp, 0, R.string.help).setShortcut('3', 'k')
				.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						Intent i = new Intent(BarTourActivity.this,
								HelpActivity.class);
						startActivity(i);
						return false;
					}
				});

		// Inflate menu resource file.
		getMenuInflater().inflate(R.menu.menu_bta, menu);
		
		
		
		

		return true;
	}

	private void startReminder() {

		Intent i = new Intent(this, Reminder.class);
		startService(i);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		
		
		
		
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_photo:
			takePhoto();
			return true;
		case R.id.menu_item_share:
			shareTour();
			return true;
		case R.id.menu_item_timer:
			
			handleTimer(item);
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void handleTimer(MenuItem item) {
		
		
		

		if (Logic.getTimerStarted() == true) {
			
			Toast toast = Toast.makeText(getApplicationContext(),
					R.string.tour_started, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();

			Logic.setTimerStarted(false);

			startReminder();

		} else {
			
			Toast toast = Toast.makeText(getApplicationContext(),
					R.string.tour_cancelled, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();

			Logic.setTimerStarted(true);
			reminder.stopTimer();

		}

	}

	private void shareTour() {
		Intent shareIntent = new Intent();

		Bitmap bm;

		try {
			Log.d(TAG, "shareTour: try");
			bm = getWholeListViewItemsToBitmap();
		} catch (Exception e) {
			e.printStackTrace();
			Log.d(TAG, "shareTour: catch");
			bm = getScreenshotBitmap();
		}

		String url = Images.Media.insertImage(getContentResolver(), bm,
				"title", null);
		Uri bmpUri = Uri.parse(url);
		shareIntent.setAction(Intent.ACTION_SEND);
		shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
		shareIntent.setType("image/*");

		startActivity(Intent.createChooser(shareIntent, "share"));
		createShareIntent();

	}

	private Bitmap getScreenshotBitmap() {
		listView.setDrawingCacheEnabled(true);
		listView.buildDrawingCache();
		Bitmap bm = listView.getDrawingCache();
		return bm;
	}

	private Bitmap getWholeListViewItemsToBitmap() throws Exception {

		int itemscount = adapter.getCount();
		int allitemsheight = 0;
		List<Bitmap> bmps = new ArrayList<Bitmap>();

		for (int i = 0; i < itemscount; i++) {

			View childView = adapter.getView(i, null, listView);
			int widthMeasureSpec = MeasureSpec.makeMeasureSpec(listView.getWidth(),
					MeasureSpec.EXACTLY);
			int heightMeasureSpec = MeasureSpec.makeMeasureSpec(listView.getWidth()/4,
					MeasureSpec.EXACTLY);
			childView.measure(widthMeasureSpec, heightMeasureSpec);

			childView.layout(0, 0, childView.getMeasuredWidth(),
					childView.getMeasuredHeight());
			childView.setDrawingCacheEnabled(true);
			childView.buildDrawingCache();
			bmps.add(childView.getDrawingCache());
			allitemsheight += childView.getMeasuredHeight();
		}

		Bitmap bigbitmap = Bitmap.createBitmap(listView.getMeasuredWidth(),
				allitemsheight, Bitmap.Config.ARGB_8888);
		Canvas bigcanvas = new Canvas(bigbitmap);

		Paint paint = new Paint();
		int iHeight = 0;

		for (int i = 0; i < bmps.size(); i++) {
			Bitmap bmp = bmps.get(i);
			bigcanvas.drawBitmap(bmp, 0, iHeight, paint);
			iHeight += bmp.getHeight();

			bmp.recycle();
			bmp = null;
		}

		return bigbitmap;

	}

	private void takePhoto() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(takePictureIntent, TAKE_PICTURE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case TAKE_PICTURE:
			try {
				Bitmap photo = (Bitmap) data.getExtras().get("data");
				File directory = new File(Environment
						.getExternalStorageDirectory().getAbsolutePath()
						+ "/DCIM/Barroulette");
				directory.mkdirs();
				String date = getFormattedDate();
				String imageFilePath = directory + "/" + date + ".jpg";
				FileOutputStream out = new FileOutputStream(imageFilePath);
				photo.compress(Bitmap.CompressFormat.JPEG, 90, out);

				Log.d(TAG, "Photo saved at: " + imageFilePath);
			} catch (Exception e) {
				e.printStackTrace();
				Toast toast = Toast.makeText(this, R.string.no_picture_taken,
						Toast.LENGTH_LONG);
				toast.show();
			}
		default:

		}
	}

	private String getFormattedDate() {
		Calendar c = Calendar.getInstance();
		String date = String.valueOf(c.get(Calendar.DATE)) + "-"
				+ String.valueOf(c.get(Calendar.MONTH)) + "-"
				+ String.valueOf(c.get(Calendar.YEAR)) + "_"
				+ String.valueOf(c.get(Calendar.HOUR)) + "-"
				+ String.valueOf(c.get(Calendar.MINUTE)) + "-"
				+ String.valueOf(c.get(Calendar.SECOND));
		return date;
	}

	@SuppressLint("ValidFragment")
	public class BarSettingDialog extends DialogFragment {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the Builder class for convenient dialog construction
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setItems(barOptions, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {

					BarItem currentItem = list.get(selectedListItem);

					switch (which) {
					case CASE_MARKERS:
						
						ArrayList<BarItem> list = new ArrayList<BarItem>();
						list.add(currentItem);
						Logic.itemListMaps = list;
						Logic.distinguish = SHOW_ON_MAP;
						Intent i = new Intent(BarTourActivity.this,
								MapsActivity.class);

						startActivity(i);
						break;
						
					case CASE_ROUTE:
						
						ArrayList<BarItem> listRoute = new ArrayList<BarItem>();
						listRoute.add(currentItem);
						Logic.itemListMapsRoute = listRoute;
						Logic.distinguish = SHOW_ROUTE;
						Intent routeIntent = new Intent(BarTourActivity.this,
								MapsActivity.class);

						startActivity(routeIntent);
						break;
						
					case CASE_CALL:
						
						String num = currentItem.getPhoneNumber();
						if (num == "" || num == " " || num == null) {
							Toast toast = Toast.makeText(
									getApplicationContext(),
									R.string.no_number, Toast.LENGTH_LONG);
							toast.show();
						} else {
							String dialnum = "tel:" + num;
							Intent call = new Intent(Intent.ACTION_DIAL, Uri
									.parse(dialnum));
							startActivity(call);
						}
						break;
					}
				}
			});

			return builder.create();
		}
	}

	@Override
	public void onBarTimerFinished() {
		Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
		vibrator.vibrate(2000);

		AlertDialog.Builder dialog = new AlertDialog.Builder(
				BarTourActivity.this);
		dialog.setTitle(R.string.dialogTitle);
		dialog.setMessage(R.string.notificationText);
		dialog.setPositiveButton(R.string.dialogOk, null);
		dialog.show();

	}

	@Override
	protected void onPause() {
		Logic.setIsVisible(false);
		unbindService(serviceConnection);
		super.onPause();
	}

	@Override
	protected void onResume() {
		Logic.setIsVisible(true);
		bindService(new Intent(BarTourActivity.this, Reminder.class),
				serviceConnection, BIND_AUTO_CREATE);
		super.onResume();
	}
	
	/**
	 * License for DragSortListView
	 * 
	 * 
	A subclass of the Android ListView component that enables drag
	and drop re-ordering of list items.

	Copyright 2012 Carl Bauer

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
	*/

}
