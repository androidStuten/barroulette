package com.example.geiles_barroulette;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class BarTourPrefsActivity extends Activity {

	private CheckBox bars, clubs, reminder, pre_drinks;
	private RadioButton private_pre_drinks, public_pre_drinks;
	private TextView bar_count;
	private RadioButton elsewhere, atHome;
	private EditText editName, editAdress;
	private Button start;
	private SeekBar drinking_level;
	private boolean bars_check = true;
	private boolean clubs_check = false;
	private int pre_drinks_place;
	private static final int PRIVATE_PLACE_HOME = Logic.PRE_DRINKS_PRIVATE_AT_HOME;
	private static final int PRIVATE_PLACE_ELSEWHERE = Logic.PRE_DRINKS_PRIVATE_ELSEWHERE;
	private static final int PUBLIC_PLACE = Logic.PRE_DRINKS_PUBLIC;
	private static final int NO_PRE_DRINKS = Logic.NO_PRE_DRINKS;
	private int level;
	private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bar_tour_prefs_2);
		setupUI();

		getDrinkingLevel();
		getPreDrinks();
		startTour();
		context = getApplicationContext();

	}

	private void setSettings() {
		Logic.setLevel(drinking_level.getProgress() / 10);
//		Logic.setReminder(reminder.isChecked());
		Logic.setBars(bars.isChecked());
		Logic.setClubs(clubs.isChecked());
		if (pre_drinks.isChecked()) {
			if (public_pre_drinks.isChecked()) {
				pre_drinks_place = PUBLIC_PLACE;
			}
			Logic.setPreDrinks(pre_drinks_place);
		} else
			Logic.setPreDrinks(NO_PRE_DRINKS);

	}

	private void startTour() {
		start.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Logic.setNewTour(true);
				if (!bars_check && !clubs_check) {
					Toast toast = Toast.makeText(getApplicationContext(),
							R.string.nothing_selected, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
					toast.show();
				} else {
					setSettings();
					Intent start = new Intent(BarTourPrefsActivity.this,
							BarTourActivity.class);
					startActivity(start);
				}

			}
		});

	}

	private void getPreDrinks() {
		private_pre_drinks.setEnabled(false);
		public_pre_drinks.setEnabled(false);
		pre_drinks_place = NO_PRE_DRINKS;
		pre_drinks.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// pre_drinks_check = true;
				if (pre_drinks.isChecked()) {
					private_pre_drinks.setEnabled(true);
					public_pre_drinks.setEnabled(true);
				} else {
					private_pre_drinks.setEnabled(false);
					public_pre_drinks.setEnabled(false);
					pre_drinks_place = NO_PRE_DRINKS;
				}

			}

		});

		private_pre_drinks.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				pre_drinks_place = PRIVATE_PLACE_HOME;
				final Dialog dialog = new Dialog(BarTourPrefsActivity.this);
				dialog.setContentView(R.layout.dialog_pre_drinks);
				dialog.setTitle(R.string.pre_drinks_where);

				Button button = (Button) dialog
						.findViewById(R.id.selectPreDrinksButton);
				elsewhere = (RadioButton) dialog
						.findViewById(R.id.preDrinksElsewhere);
				atHome = (RadioButton) dialog
						.findViewById(R.id.preDrinksAtHome);
				editName = (EditText) dialog
						.findViewById(R.id.preDrinkElsewhereName);
				editAdress = (EditText) dialog
						.findViewById(R.id.preDrinkElsewhereAdress);
				elsewhere.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						pre_drinks_place = PRIVATE_PLACE_ELSEWHERE;
						editName.setEnabled(true);
						editAdress.setEnabled(true);

					}
				});

				atHome.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						pre_drinks_place = PRIVATE_PLACE_HOME;
						editName.setEnabled(false);
						editAdress.setEnabled(false);

					}
				});

				button.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						if (atHome.isChecked()) {
							if (Logic.homeName == null) {
								Logic.setNoAddressForPreDrinkAtHomeIsSet(false);
								Intent settingsIntent = new Intent(
										BarTourPrefsActivity.this,
										SettingsActivity.class);
								startActivity(settingsIntent);
							} else {
								Logic.setPrivatePreDrinkLocation(Logic.homeName);
							}
						} else {
							String name = editName.getText().toString();
							String adress = editAdress.getText().toString();

							if (name == null || name.length() < 1) {
								Toast toast = Toast.makeText(context,
										R.string.empty_name, Toast.LENGTH_LONG);
								toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
								toast.show();
								return;
							}
							// Logic.setPrivatePreDrinkLocation(name);
							Logic.elsewhereName = name;
							if (adress.length() > 1) {
								if (!adress.contains(", ")) {
									Toast toast = Toast.makeText(context,
											R.string.no_comma,
											Toast.LENGTH_LONG);
									toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
									toast.show();
									return;
								}
								Logic.elsewhereAdress = adress;
							}

						}
						// String name = editPlace.getText().toString();
						// Logic.setPrivatePreDrinkLocation(name);
						dialog.dismiss();

					}
				});

				dialog.show();
			}

		});

		public_pre_drinks.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				pre_drinks_place = PUBLIC_PLACE;

			}

		});
	}

	private void getDrinkingLevel() {
		// sets a default of 2 bars
		level = 2;
		drinking_level.setProgress(level * 10);
		bar_count.setText(String.valueOf(level));

		drinking_level
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {

					}

					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser) {
						bar_count.setText(String.valueOf(progress / 10));
					}

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						// level = drinking_level.getProgress() / 10; // allows
						// 0
						// to 10
						// bars
					}
				});

	}

	private void setupUI() {
		bars = (CheckBox) findViewById(R.id.bars);
		clubs = (CheckBox) findViewById(R.id.clubs);

//		reminder = (CheckBox) findViewById(R.id.reminder);
		pre_drinks = (CheckBox) findViewById(R.id.pre_drinks);
		private_pre_drinks = (RadioButton) findViewById(R.id.private_pre_drinks);
		public_pre_drinks = (RadioButton) findViewById(R.id.public_pre_drinks);
		start = (Button) findViewById(R.id.lets_go);
		drinking_level = (SeekBar) findViewById(R.id.adjustEveningBar);
		bar_count = (TextView) findViewById(R.id.barCount);

	}

	public interface BarSettingDialogListener {

		public void onDialogPositiveClick(DialogFragment dialog);

	}

	// @SuppressLint("ValidFragment")
	// public class PreDrinkSettingDialog extends DialogFragment implements
	// BarSettingDialogListener {
	//
	// @Override
	// public Dialog onCreateDialog(Bundle savedInstanceState) {
	// AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	//
	// LayoutInflater inflater = getActivity().getLayoutInflater();
	//
	// builder.setView(inflater.inflate(R.layout.dialog_pre_drinks, null))
	//
	// .setPositiveButton(R.string.dialogOk,
	// new DialogInterface.OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int id) {
	// RadioButton atHome = (RadioButton) findViewById(R.id.preDrinksAtHome);
	// if(atHome.isChecked()){
	// EditText place = (EditText) findViewById(R.id.preDrinkPlace);
	// String name = place.getText().toString();
	// Logic.setPrivatePreDrinkLocation(name);
	// }else{
	// if(Logic.homeName == null){
	// Toast toast = Toast.makeText(getApplicationContext(),
	// R.string.no_adress_entered, Toast.LENGTH_LONG);
	// toast.show();
	// }else{
	// Logic.setPrivatePreDrinkLocation(Logic.homeName);
	// }
	//
	// }
	//
	// }
	// });
	//
	// return builder.create();
	// }
	//
	// @Override
	// public void onDialogPositiveClick(DialogFragment dialog) {
	//
	// }
	//
	// }

}