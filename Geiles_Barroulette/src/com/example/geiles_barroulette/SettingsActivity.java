package com.example.geiles_barroulette;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends Activity {

	private static final int TAKE_ME_HOME = 4;

	private Button back, save;
	private EditText street, city, name;
	private TextView savedAdress;
	private String streetName, cityName, nameName;
	private String adressNow = "Aktuelle Adresse: ";

	private ArrayList<AdressItem> taskItemList;

	private AdressDatabase adressDatabase;
	private Cursor dbCursor;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setupUIComponents();
		setupAndConnectDataSource();
		connectToDatabase();
		displayActualAdress();
		saveAddress();
		goBack();

	}

	private void displayActualAdress() {
		dbCursor = adressDatabase.getAdress();

		if (dbCursor.moveToFirst()) {

			String name = dbCursor.getString(AdressDatabase.COLUMN_NAME_INDEX);
			String street = dbCursor
					.getString(AdressDatabase.COLUMN_STREET_INDEX);
			String city = dbCursor.getString(AdressDatabase.COLUMN_CITY_INDEX);

			AdressItem currentItem = new AdressItem(name, street, city);
			taskItemList.add(0, currentItem);

			savedAdress.setText(adressNow + name + ", " + street + ", " + city);
		}
	}

	private void goBack() {
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();

			}

		});

	}

	private void saveAddress() {
		save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				addAddressItem();
				if (Logic.getTakeMeHomeIsClicked()) {
					Logic.setTakeMeHome(false);
					Logic.distinguish = TAKE_ME_HOME;
					Intent t = new Intent(SettingsActivity.this,
							MapsActivity.class);
					startActivity(t);
				} else if (Logic.getAddressForPreDrinkAtHomeIsSet() == false) {
					Logic.setNoAddressForPreDrinkAtHomeIsSet(true);
					finish();
				}

			}

		});

	}

	private void connectToDatabase() {
		// Instantiate new Database Adapter object
		adressDatabase = new AdressDatabase(this);
		// Open database connection
		adressDatabase.open();

	}

	@Override
	protected void onDestroy() {
		adressDatabase.close();
		super.onDestroy();
	}

	private void setupUIComponents() {
		setContentView(R.layout.settings_layout);

		back = (Button) findViewById(R.id.back);
		street = (EditText) findViewById(R.id.street);
		city = (EditText) findViewById(R.id.city);
		save = (Button) findViewById(R.id.save);
		savedAdress = (TextView) findViewById(R.id.saved_adress);
		name = (EditText) findViewById(R.id.name);
	}

	private void setupAndConnectDataSource() {

		// Setup the Arraylist and create a instance of our own adapter
		taskItemList = new ArrayList<AdressItem>();

	}

	// add a todo item to the database
	private void addAddressItem() {

		// retrieve the task data from the interface
		nameName = name.getText().toString();
		streetName = street.getText().toString();
		cityName = city.getText().toString();

		checkForSpecialChars();

		// prevent adding empty items...
		if (nameName.length() < 1 || streetName.length() < 1
				|| cityName.length() < 1) {
			Toast toast = Toast.makeText(getApplicationContext(),
					R.string.empty_fields, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
			toast.show();
			return;
		}

		erasePreviousAdress();

		// create a new Task item
		AdressItem task = new AdressItem(nameName, streetName, cityName);
		savedAdress.setText(nameName + ", " + streetName + ", " + cityName);
		Logic.homeAdress = streetName + ", " + cityName;
		Logic.homeName = nameName;

		adressDatabase.insertAdress(task);

		// reset the input fields
		name.setText("");
		street.setText("");
		city.setText("");
	}

	private void checkForSpecialChars() {
		if (streetName.contains(" ")) {
			streetName.replace(" ", "+");
		}
	}

	private void erasePreviousAdress() {
		dbCursor = adressDatabase.getAdress();
		if (dbCursor.moveToFirst()) {
			do {

				String nameName = dbCursor
						.getString(AdressDatabase.COLUMN_NAME_INDEX);
				String streetName = dbCursor
						.getString(AdressDatabase.COLUMN_STREET_INDEX);
				String cityName = dbCursor
						.getString(AdressDatabase.COLUMN_CITY_INDEX);

				AdressItem currentItem = new AdressItem(nameName, streetName,
						cityName);
				adressDatabase.removeAdress(currentItem);
			} while (dbCursor.moveToNext());
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
