package com.example.geiles_barroulette;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;



public class MainActivity extends Activity {

	private static final int TAKE_ME_HOME = 4;
	private static final int KEBAP = 2;
	private static final int BEER_TO_GO = 3;

	private ImageView maskotzchenGuidance, maskotzchenVomitting;
	private ImageButton randomBarButton, planButton,
			kebapButton, takeMeHomeButton, beerButton,
			drunkenessAnalyzerButton, continueTourButton;
	private int menuSettings;
	private int menuHelp;

	private EditText name, street, city;
	private AdressDatabase adressDatabase;
	private Cursor dbCursor;
//	private BarTourDatabase barTourDatabase;

	private Logic logic;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		adressDatabase = new AdressDatabase(getApplicationContext());
		setupUI();
		setupOnClicks();
		loadAdressFromDatabase();
	}

//	private void checkGooglePlayServices() {
//		 int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(MainActivity.this);
//	      if(resultCode != ConnectionResult.SUCCESS)
//	      {
//	          Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 69);
//	          dialog.setCancelable(true);
//	          dialog.show();
//	      }
//	}

	private void loadAdressFromDatabase() {
		adressDatabase = new AdressDatabase(this);
		adressDatabase.open();
		dbCursor = adressDatabase.getAdress();
		
		if (dbCursor.moveToFirst()) {

				String name = dbCursor.getString(AdressDatabase.COLUMN_NAME_INDEX);
				String street = dbCursor.getString(AdressDatabase.COLUMN_STREET_INDEX);
				String city = dbCursor.getString(AdressDatabase.COLUMN_CITY_INDEX);
				
				Logic.homeName = name;
				Logic.homeAdress = street + ", " + city;
		}
	}

	private void setupOnClicks() {
		randomBarButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// DialogFragment dialog = new ChooseDistanceDialog();
				// dialog.show(getFragmentManager(), "choose distance");
				changeMaskotzchen();
				randomBarStart();
			}
		});

		planButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				changeMaskotzchen();
				Intent i = new Intent(MainActivity.this,
						BarTourPrefsActivity.class)
						.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
			}
		});

		continueTourButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				if(barTourDatabase.getAllBarItems() != null){
					changeMaskotzchen();
				Logic.setNewTour(false);
				Intent i = new Intent(MainActivity.this,
						BarTourActivity.class);
				startActivity(i);
//				}else{
//					Toast toast = Toast.makeText(
//							getApplicationContext(),
//							R.string.no_number, Toast.LENGTH_LONG);
//					toast.show();
//				}
				
			}
		});

		kebapButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				changeMaskotzchen();
				Logic.distinguish = KEBAP;
				Logic.itemListKebap = logic.getKebapList();
				Intent k = new Intent(MainActivity.this, MapsActivity.class);
				startActivity(k);
			}
		});

		takeMeHomeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				changeMaskotzchen();
				if (Logic.homeAdress == null) {
					Logic.setTakeMeHome(true);
					Logic.distinguish = TAKE_ME_HOME;
					Intent t = new Intent(MainActivity.this, SettingsActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(t);
					

				}else{
					Logic.distinguish = TAKE_ME_HOME;
					Intent t = new Intent(MainActivity.this, MapsActivity.class);
					startActivity(t);
				}

			}
		});

		beerButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				changeMaskotzchen();
				Logic.distinguish = BEER_TO_GO;
				Logic.itemListBeerToGo = logic.getBeerToGoList();
				Intent b = new Intent(MainActivity.this, MapsActivity.class);
				startActivity(b);
			}
		});

		drunkenessAnalyzerButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				changeMaskotzchen();
				Intent d = new Intent(MainActivity.this,
						DrunkenessAnalyzerActivity.class);
				startActivity(d);
			}
		});

	}

	private void changeMaskotzchen() {
		maskotzchenGuidance.setVisibility(View.INVISIBLE);
		maskotzchenVomitting.setVisibility(View.VISIBLE);
	}

	private void setupUI() {
		randomBarButton = (ImageButton) findViewById(R.id.randomBarButton);
		planButton = (ImageButton) findViewById(R.id.planButton);
		// documentationButton = (ImageButton)
		// findViewById(R.id.documentationButton);
		kebapButton = (ImageButton) findViewById(R.id.kebapButton);
		takeMeHomeButton = (ImageButton) findViewById(R.id.takeMeHomeButton);
		beerButton = (ImageButton) findViewById(R.id.beerButton);
		drunkenessAnalyzerButton = (ImageButton) findViewById(R.id.drunkenessAnalyzerButton);
		maskotzchenVomitting = (ImageView) findViewById(R.id.maskotzchenVomitting);
		maskotzchenGuidance = (ImageView) findViewById(R.id.maskotzchenGuidance);
		continueTourButton = (ImageButton) findViewById(R.id.continueTour);
		logic = new Logic(getApplicationContext());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(0, menuSettings, 0, R.string.settings).setShortcut('1', 'x')
				.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem arg0) {
						Intent i = new Intent(MainActivity.this,
								SettingsActivity.class)
								.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(i);
						return false;
					}

				});

		menu.add(0, menuHelp, 0, R.string.help).setShortcut('2', 'f')
				.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						Intent i = new Intent(MainActivity.this,
								HelpActivity.class);
						startActivity(i);
						return false;
					}
				});

		return true;
	}

	private void randomBarStart() {
		Logic.setLevel(1);
		Logic.setClubs(false);
		Logic.setBars(true);
		Logic.setPreDrinks(0);
		Logic.setNewTour(true);
		Intent bar = new Intent(MainActivity.this, BarTourActivity.class);
		startActivity(bar);
	}

	// @SuppressLint("ValidFragment")
	// public class ChooseDistanceDialog extends DialogFragment {
	// @Override
	// public Dialog onCreateDialog(Bundle savedInstanceState) {
	// // Use the Builder class for convenient dialog construction
	// AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	// builder.setMessage(R.string.search_area)
	// .setPositiveButton(R.string.yes,
	// new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog,
	// int id) {
	// // bar in der nähe
	// }
	// })
	// .setNegativeButton(R.string.no,
	// new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog,
	// int id) {
	//
	// }
	// });
	//
	// return builder.create();
	// }
	// }

//	@SuppressLint("ValidFragment")
//	public class EnterAdressDialog extends DialogFragment {
//		@Override
//		public Dialog onCreateDialog(Bundle savedInstanceState) {
//			// Use the Builder class for convenient dialog construction
//			setContentView(R.layout.settings_layout);
//			setupUI();
//			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//			builder.setMessage(R.string.adress)
//					.setPositiveButton(R.string.dialogOk,
//							new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog,
//										int id) {
//
//									String streetName = street.getText()
//											.toString();
//									String cityName = city.getText().toString();
//									String nameName = name.getText().toString();
//									
//									Logic.distinguish = TAKE_ME_HOME;
//
//									Intent t = new Intent(MainActivity.this,
//											MapsActivity.class);
//									startActivity(t);
//								}
//							})
//					.setNegativeButton(R.string.cancel,
//							new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog,
//										int id) {
//									dialog.dismiss();
//								}
//							});
//
//			return builder.create();
//		}
//
//		private void setupUI() {
//			name = (EditText) findViewById(R.id.name);
//			street = (EditText) findViewById(R.id.street);
//			city = (EditText) findViewById(R.id.city);
//
//		}
//	}

}