package com.example.geiles_barroulette;

import java.util.ArrayList;
import java.util.List;

import com.mobeta.android.dslv.DragSortListView;
import com.mobeta.android.dslv.DragSortListView.DragListener;
import com.mobeta.android.dslv.DragSortListView.DropListener;
import com.mobeta.android.dslv.DragSortListView.RemoveListener;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.sax.StartElementListener;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.View.OnDragListener;
//import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class BarTourAdapter extends ArrayAdapter<BarItem> {

	private List<BarItem> list;
	private Context context;
	private static final String TAG = "Adapter";
	private Typeface font;

	public BarTourAdapter(Context context, ArrayList<BarItem> list) {
		super(context, R.layout.bar_list_item, list);

		this.context = context;
		this.list = list;
		font = Typeface.createFromAsset(context.getAssets(), "fonts/alba.ttf");
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.bar_list_item, null);
		}
		BarItem item = list.get(position);

		if (item != null) {
			TextView name = (TextView) view.findViewById(R.id.barName);
			TextView openingHours = (TextView) view
					.findViewById(R.id.barOpeningHours);
			ImageView leer = (ImageView) view.findViewById(R.id.leerBackground);
			ImageView predrink = (ImageView) view
					.findViewById(R.id.predrinkBackground);
			ImageView club = (ImageView) view.findViewById(R.id.clubBackground);
			
			name.setTypeface(font);
			openingHours.setTypeface(font);
			name.setText(item.getName());

			if (item.getOpeninghours() != null || item.getOpeninghours() != "") {
				openingHours.setText(item.getOpeninghours());
			}

			int preDrinks = Logic.getPreDrinks();

			if (item.getIsPreDrink()) {
				leer.setVisibility(View.INVISIBLE);
				predrink.setVisibility(View.VISIBLE);
				club.setVisibility(View.INVISIBLE);
				if (preDrinks == Logic.PRE_DRINKS_PRIVATE_AT_HOME
						|| (preDrinks == Logic.PRE_DRINKS_PRIVATE_ELSEWHERE && item
								.getAdress()[0].length() > 1)) {
					openingHours.setText(item.getAdress()[0] + ", "
							+ item.getAdress()[1]);
				}

			} else {
				leer.setVisibility(View.VISIBLE);
				predrink.setVisibility(View.INVISIBLE);
				club.setVisibility(View.INVISIBLE);
			}

			if (item.getIsClub()) {
				leer.setVisibility(View.INVISIBLE);
				predrink.setVisibility(View.INVISIBLE);
				club.setVisibility(View.VISIBLE);
			}
			
		}

		return view;
	}
	

}