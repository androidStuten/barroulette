package com.example.geiles_barroulette;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AdressDatabase {

	// Parameters for the database
	private static final String DATABASE_NAME = "address.db";
	private static final int DATABASE_VERSION = 1;

	// Name of database table
	private static final String DATABASE_TABLE = "Address";
	// Set the names for database columns in table (we only have two columns)
	public static final String KEY_ID = "_id";
	public static final String KEY_NAME = "name";
	public static final String KEY_STREET = "street";
	public static final String KEY_CITY = "city";

	// Stor index of task-column
	public static final int COLUMN_NAME_INDEX = 1;
	public static final int COLUMN_STREET_INDEX = 2;
	public static final int COLUMN_CITY_INDEX = 3;

	public static final String TAG = "Database";

	private AdressDBOpenHelper dbHelper;
	private SQLiteDatabase db;

	public AdressDatabase(Context context) {
		dbHelper = new AdressDBOpenHelper(context, DATABASE_NAME, null,
				DATABASE_VERSION);

	}

	private class AdressDBOpenHelper extends SQLiteOpenHelper {
		private static final String DATABASE_CREATE = "create table "
				+ DATABASE_TABLE + " (" 
				+ KEY_ID + " integer primary key autoincrement, " 
				+ KEY_NAME + " text not null, " 
				+ KEY_STREET + " text, " 
				+ KEY_CITY + " text);";

		public AdressDBOpenHelper(Context c, String dbname,
				SQLiteDatabase.CursorFactory factory, int version) {
			super(c, dbname, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		}
	}

	public void open() throws SQLException {
		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLException e) {
			db = dbHelper.getReadableDatabase();
		}
	}

	public void close() {
		db.close();
	}

	public long insertAdress(AdressItem item) {
		ContentValues newAddressValues = new ContentValues();

		// add the data form the TodoItem object
		newAddressValues.put(KEY_NAME, item.getName());
		newAddressValues.put(KEY_STREET, item.getStreet());
		newAddressValues.put(KEY_CITY, item.getCity());

		// insert into the database
		return db.insert(DATABASE_TABLE, null, newAddressValues);
	}

	public void removeAdress(AdressItem item) {
		String whereClause = KEY_NAME + " = '" + item.getName() + "' AND "
				+ KEY_STREET + " = '" + item.getStreet() + "' AND " + KEY_CITY
				+ " = '" + item.getCity() + "'";

		db.delete(DATABASE_TABLE, whereClause, null);
	}

	public Cursor getAdress() {
		return db.query(DATABASE_TABLE, new String[] {KEY_ID, KEY_NAME,
				KEY_STREET, KEY_CITY}, null, null, null, null, null);

	}
}