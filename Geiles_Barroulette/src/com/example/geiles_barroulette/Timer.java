package com.example.geiles_barroulette;

import android.os.CountDownTimer;

public class Timer {

	public static final int TIME_PER_BAR = 3000;

	// L�uft der Timer gerade? Auslagerung der Werte in Configdatei
	public static final int TIMERSTATUS_STOPPED = 0;
	public static final int TIMERSTATUS_RUNNING = 1;
	public static final int TIMERSTATUS_CANCELLED = 2;
	



	// Schl�ssel f�r das Contentvalues-Objekt der MIN:SEC Darstellung
	// werden hier hinterlegt, Fehler zu vermeiden
	public static final String KEY_MINUTES = "Minuten";
	public static final String KEY_SECONDS = "Sekunden";
	
	private MyCountDownTimer myCountDownTimer;
	private OnBarTimerStatusChangedListener onBarTimerStatusChangedListener;

	private int status = TIMERSTATUS_STOPPED;

	public Timer(
			OnBarTimerStatusChangedListener onBarTimerStatusChangedListener,
			int timePerBar) {
		this.onBarTimerStatusChangedListener = onBarTimerStatusChangedListener;

		myCountDownTimer = new MyCountDownTimer(60000, 1000);
	}

	public int getTimerStatus() {
		return status;
	}

	public void start() {
		myCountDownTimer.start();
	}

	public void cancel() {
		myCountDownTimer.cancel();
		status = TIMERSTATUS_CANCELLED;
	}

	class MyCountDownTimer extends CountDownTimer {

		public MyCountDownTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);

		}

		@Override
		public void onFinish() {

			status = TIMERSTATUS_STOPPED;

			onBarTimerStatusChangedListener.onBarTimerFinished();
			
			if(status != TIMERSTATUS_CANCELLED){
			this.start();
			}
		}

		@Override
		public void onTick(long millisUntilFinished) {

			status = TIMERSTATUS_RUNNING;

		}

	}

}
