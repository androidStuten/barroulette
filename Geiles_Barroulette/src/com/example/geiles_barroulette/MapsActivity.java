package com.example.geiles_barroulette;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapsActivity extends FragmentActivity {

	private static final String CURRENT_LOCATION = "Deine Position";
	private static final String TAG = "BarTourActivity";
	
	private GoogleMap map;
	private Location srcAddress;
	private int distinguisher;
	private ArrayList<BarItem> distinguishArray;
	private ArrayList<Double> jsonLat;
	private ArrayList<Double> jsonLong;
	private String downloadedtext;
	private BarItem item;
	private PolylineOptions route;
	private int [] drawables;

	private static final int SEPARATOR_BAR_ROUTE = 1;
	private static final int SEPARATOR_KEBAP = 2;
	private static final int SEPARATOR_BEER_TO_GO = 3;
	private static final int SEPARATOR_TAKE_ME_HOME = 4;

	private AdressDatabase adressDatabase;
	private ArrayList<AdressItem> adress;
	private Cursor dbCursor;
	private String privateAdress;

	private double lat;
	private double lng;
	private JSONArray stepsArray;
	private Logic logic;
	
	private String distance;
	private String duration;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		logic = new Logic(getApplicationContext());
		getDistinguisher();
		//getGPSLocation();
		setupMap();

	}

	

	private void getDistinguisher() {
		this.distinguisher = Logic.distinguish;
		switch (distinguisher) {
		case 0:
			distinguishArray = Logic.itemListMaps;
			break;
		case SEPARATOR_BAR_ROUTE:
			getGPSLocation();
			distinguishArray = Logic.itemListMapsRoute;
			break;
		case SEPARATOR_KEBAP:
			distinguishArray = logic.getKebapList();
			break;
		case SEPARATOR_BEER_TO_GO:
			distinguishArray = logic.getBeerToGoList();
			break;
		case SEPARATOR_TAKE_ME_HOME:
			getGPSLocation();
			getHomeAdressFromDatabase();
			distinguishArray = new ArrayList<BarItem>();
			BarItem home = new BarItem("home");
			distinguishArray.add(home);
			break;
			// no break on purpose
		
		}
	}

	private void getHomeAdressFromDatabase() {
		// adress = new ArrayList<AdressItem>();
		// adressDatabase = new AdressDatabase(this);
		// adressDatabase.open();
		// updateList();
		// AdressItem item = adress.get(adress.size());
		// privateAdress = item.getStreet() + ", " + item.getCity();

		privateAdress = Logic.homeAdress;
	}

	public void updateList() {

		// Cursor updates itself (gets all tasks from Database again)
		// Necessary, because we update ArrayList, also when populateToDolist
		// was not called directly before this function
		dbCursor = adressDatabase.getAdress();

		// ArrayList is emptied... We always start from scratch
		adress.clear();

		// Go to first item in the cursor
		if (dbCursor.moveToFirst()) {
			do {
				// Gets the Task in the current row from the Cursor
				// Note: We get the column index from the Adapter class! Makes
				// debugging / changes easier...
				String name = dbCursor
						.getString(AdressDatabase.COLUMN_NAME_INDEX);
				String street = dbCursor
						.getString(AdressDatabase.COLUMN_STREET_INDEX);
				String city = dbCursor.getString(dbCursor
						.getColumnIndex(AdressDatabase.KEY_CITY));

				AdressItem currentItem = new AdressItem(name, street, city);
				// Push to beginning of ArrayList
				adress.add(0, currentItem);
			}
			// returns true, if there is a new row, otherwise false, and we
			// continue with notifying the View that data has changed
			while (dbCursor.moveToNext());
		}
	}

	private Location getGPSLocation() {
		LocationManager manager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		String provider = LocationManager.GPS_PROVIDER;
		srcAddress = manager.getLastKnownLocation(provider);
		return srcAddress;
	}

	private void loadMarkers() {

		for (int i = 0; i < distinguishArray.size(); i++) {
			item = distinguishArray.get(i);
			List<Address> address;
			Geocoder geocoder = new Geocoder(getApplicationContext());

			try {
				String formatAdr = "";
				if (distinguisher != SEPARATOR_TAKE_ME_HOME) {
					formatAdr = item.getAdress()[0] + ", "
							+ item.getAdress()[1];
				} else if (distinguisher == SEPARATOR_TAKE_ME_HOME) {
					formatAdr = privateAdress;
				}

				address = geocoder.getFromLocationName(formatAdr, 2);
				if (address != null) {

					Address location = address.get(0);
					lat = location.getLatitude();
					lng = location.getLongitude();

					if(distinguisher != SEPARATOR_TAKE_ME_HOME){
					map.addMarker(new MarkerOptions()
							.position(new LatLng(lat, lng))
							.title(item.getName())
							.snippet(item.getOpeninghours()));
					}else if(distinguisher == SEPARATOR_TAKE_ME_HOME){
						map.addMarker(new MarkerOptions()
						.position(new LatLng(lat, lng))
						.title("Dein Zuhause: " + Logic.homeName)
						.snippet(Logic.homeAdress));
					}
					
					if (distinguisher == SEPARATOR_BAR_ROUTE
							|| distinguisher == SEPARATOR_TAKE_ME_HOME) {
						map.addMarker(new MarkerOptions().position(
								new LatLng(srcAddress.getLatitude(), srcAddress
										.getLongitude())).title(
								CURRENT_LOCATION).icon(BitmapDescriptorFactory
										.fromResource(R.drawable.vomitting)));
						DataDownloadTask task = new DataDownloadTask();
						task.execute();
//					} else {
//						Toast toast = Toast.makeText(
//								getApplicationContext(),
//								R.string.no_location_found, Toast.LENGTH_LONG);
//						toast.show();
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	private void zoomToMyPosition() {

		Location me = srcAddress;
		if(me != null){
		CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(me
				.getLatitude(), me.getLongitude()));
		CameraUpdate zoom = CameraUpdateFactory.zoomTo(17);
		map.moveCamera(center);
		map.animateCamera(zoom);
		}else{
			Toast toast = Toast.makeText(
					getApplicationContext(),
					R.string.no_location_found, Toast.LENGTH_LONG);
			toast.show();
			
		}
		

		
	}

	private void setupMap() {
		if (map == null) {
			// Try to obtain the map from the SupportMapFragment.
			map = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (map != null) {
				map.setMyLocationEnabled(true);
				loadMarkers();
				if (distinguisher == SEPARATOR_BAR_ROUTE || distinguisher == SEPARATOR_TAKE_ME_HOME){
					zoomToMyPosition();
				}
			}
		} else {
			map.clear();
			loadMarkers();
		}
	}

	private void processJson() throws JSONException {
		jsonLat = new ArrayList<Double>();
		jsonLat.add(srcAddress.getLatitude());
		jsonLong = new ArrayList<Double>();
		jsonLong.add(srcAddress.getLongitude());

//		JSONObject jsonObject = new JSONObject(downloadedtext);
//
//		JSONArray routesArray = jsonObject.getJSONArray("routes");
//		JSONObject route = routesArray.getJSONObject(0);
//		JSONArray legs = route.getJSONArray("legs");
//		JSONObject leg = legs.getJSONObject(0);
//
		
		

		JSONObject json = new JSONObject(downloadedtext);
		JSONObject jsonRoute = json.getJSONArray("routes").getJSONObject(0);
		JSONObject leg = jsonRoute.getJSONArray("legs").getJSONObject(0);
		JSONArray steps = leg.getJSONArray("steps");
		
		JSONObject durationObject = leg.getJSONObject("duration");
		duration = durationObject.getString("text");
		
		JSONObject distanceObject = leg.getJSONObject("distance");
		distance = distanceObject.getString("text");
		
		for (int i = 0; i < steps.length(); i++) {
			JSONObject endLocation = new JSONObject(steps.getJSONObject(i)
					.getString("end_location"));
			double lat = endLocation.getDouble("lat");
			jsonLat.add(lat);
			double lng = endLocation.getDouble("lng");
			jsonLong.add(lng);
		}
	}

	private void loadData() {
		try {
			downloadedtext = downloadFile();
			processJson();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String downloadFile() throws IllegalStateException,
			MalformedURLException, ProtocolException, IOException {

		String toReturn = "";
		URL url = new URL(
				"http://maps.googleapis.com/maps/api/directions/json?origin="
						+ srcAddress.getLatitude() + ","
						+ srcAddress.getLongitude() + "&destination=" + lat
						+ "," + lng + "&sensor=false&mode=walking");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		int responseCode = conn.getResponseCode();

		if (responseCode == HttpURLConnection.HTTP_OK) {

			InputStream is = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			String line;

			while ((line = br.readLine()) != null) {
				toReturn += line;
			}

			br.close();
			is.close();
			conn.disconnect();

		} else {
			throw new IllegalStateException("HTTP response: " + responseCode);
		}
		return toReturn;
	}

	private class DataDownloadTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			Toast toast = Toast.makeText(
					getApplicationContext(),
					R.string.load_data, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			toast.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			loadData();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			Toast toast = Toast.makeText(
					getApplicationContext(),
					"Distanz zum Ziel: " + distance + "\n" + "Zu ben�tigende Zeit: " + duration, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			toast.show();
			
			route = new PolylineOptions();
			for (int i = 0; i < jsonLat.size(); i++) {
				route.add(new LatLng(jsonLat.get(i), jsonLong.get(i)));
			}
			Polyline polyline = map.addPolyline(route);
			polyline.setColor(Color.BLUE);

		}

	}

	@Override
	protected void onResume() {
		setupMap();
		super.onResume();
	}

}