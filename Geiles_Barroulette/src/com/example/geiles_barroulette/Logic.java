package com.example.geiles_barroulette;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class Logic {

	private static int level, preDrinks, barsInDatabase, clubsInDatabase,
			preDrinksInDatabase;
	private static boolean bars, clubs, reminder,
			barListIsInitialized = false, clubListIsInitialized = false,
			preDrinkListIsInitialized = false, kebapListIsInitialized = false,
			beerToGoListIsInitialized = false, takeMeHomeIsClicked = false, noAddressForPreDrinkAtHomeYet = true,
			timerStarted = true;
	private static boolean mIsVisible = false;

	public static ArrayList<BarItem> barList, clubList, preDrinkList,
			itemListKebap, itemListBeerToGo, itemListMaps, itemListMapsRoute;
	public static int distinguish;
	public static String homeAdress;
	public static String homeName;
	public static String elsewhereAdress;
	public static String elsewhereName;
	
	private static boolean newTour;

	private Context ctx;

	private static final String TAG_BARS = "bars";
	private static final String TAG_CLUBS = "clubs";
	private static final String TAG_PRE_DRINKS = "predrinkingpublic";
	private static final String TAG_KEBAP = "kebap";
	private static final String TAG_BEER_TO_GO = "beertogo";
	public static final int SELECTOR_BAR_LIST = 1;
	public static final int SELECTOR_CLUB_LIST = 2;
	public static final int SELECTOR_PRE_DRINK_LIST = 3;
	public static final int SELECTOR_KEBAP_LIST = 4;
	public static final int SELECTOR_BEER_TO_GO_LIST = 5;
	public static final int NO_PRE_DRINKS = 0;
	public static final int PRE_DRINKS_PRIVATE_AT_HOME = 1;
	public static final int PRE_DRINKS_PRIVATE_ELSEWHERE = 3;
	public static final int PRE_DRINKS_PUBLIC = 2;
	

	private static String privatePreDrinkLocation;
	private static String adress = "";

	public Logic(Context ctx) {
		this.ctx = ctx;
		
	}

	// selector ==1 for only bar, == 2 for only clubs, == 3 for
	// preDrinks
	private void initializeLists(int selector) {
		if (selector == SELECTOR_BAR_LIST && barListIsInitialized == false) {
			barList = new ArrayList<BarItem>();
			readWholeBarList();

		} else if (selector == SELECTOR_CLUB_LIST && clubListIsInitialized == false) {
			clubList = new ArrayList<BarItem>();
			readWholeClubList();

		} else if (selector == SELECTOR_PRE_DRINK_LIST && preDrinkListIsInitialized == false) {
			preDrinkList = new ArrayList<BarItem>();
			readWholePreDrinkList();

		} else if (selector == SELECTOR_KEBAP_LIST && kebapListIsInitialized == false) {
			itemListKebap = new ArrayList<BarItem>();
			readWholeKebapList();

		} else if (selector == SELECTOR_BEER_TO_GO_LIST && beerToGoListIsInitialized == false) {
			itemListBeerToGo = new ArrayList<BarItem>();
			readWholeBeerToGoList();
		}
	}

	// getterMethods for all kinds of lists

	public BarItem getRandomBar() {
		initializeLists(SELECTOR_BAR_LIST);
		BarItem bar = barList.get((int) (Math.random() * (barsInDatabase)));
		return bar;
	}

	public BarItem getRandomClub() {
		initializeLists(SELECTOR_CLUB_LIST);
		BarItem club = clubList.get((int) (Math.random() * (clubsInDatabase)));
		return club;
	}

	public BarItem getRandomPreDrinkPlace() {
		initializeLists(SELECTOR_PRE_DRINK_LIST);
		BarItem preDrinkPlace = preDrinkList
				.get((int) (Math.random() * (preDrinksInDatabase)));
		return preDrinkPlace;
	}

	public ArrayList<BarItem> getKebapList() {
		initializeLists(SELECTOR_KEBAP_LIST);
		return itemListKebap;
	}

	public ArrayList<BarItem> getBeerToGoList() {
		initializeLists(SELECTOR_BEER_TO_GO_LIST);
		return itemListBeerToGo;
	}

	// methods used in "initializeLists" depending on selector

	private void readWholeClubList() {
		readJSONList(TAG_CLUBS, R.raw.clubs, clubList, SELECTOR_BAR_LIST);
		clubsInDatabase = clubList.size();
		 clubListIsInitialized = true;
	}

	private void readWholeBarList() {
		readJSONList(TAG_BARS, R.raw.bars, barList, SELECTOR_CLUB_LIST);
		barsInDatabase = barList.size();
		 barListIsInitialized = true;
	}

	private void readWholePreDrinkList() {
		readJSONList(TAG_PRE_DRINKS, R.raw.predrinkspublic, preDrinkList, SELECTOR_PRE_DRINK_LIST);
		preDrinksInDatabase = preDrinkList.size();
		 preDrinkListIsInitialized = true;
	}

	private void readWholeKebapList() {
		readJSONList(TAG_KEBAP, R.raw.kebap, itemListKebap, SELECTOR_KEBAP_LIST);
	}

	private void readWholeBeerToGoList() {
		readJSONList(TAG_BEER_TO_GO, R.raw.beertogo, itemListBeerToGo, SELECTOR_BEER_TO_GO_LIST);
	}

	private void readJSONList(String Tag, int path, ArrayList<BarItem> list,
			int selector) {
		String text = readJSON(path);

		try {
			JSONObject json = new JSONObject(text);
			JSONArray jsonArray = json.getJSONArray(Tag);

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);

				String name = jsonObject.getString("NAME");
				String adress = jsonObject.getString("ADDRESS");
				BarItem item = new BarItem(name);
				if (selector == SELECTOR_BAR_LIST || selector == SELECTOR_CLUB_LIST || selector == SELECTOR_KEBAP_LIST 
						|| selector == SELECTOR_BEER_TO_GO_LIST) {
					String openingHours = jsonObject.getString("OPENINGHOURS");
					item.setOpeninghours(openingHours);
				}
				if (selector == SELECTOR_BAR_LIST || selector == SELECTOR_CLUB_LIST) {
					String phone = jsonObject.getString("PHONE");
					item.setPhoneNumber(phone);
				}
				if (list == clubList){
					item.setIsClub(true);
				}

				String[] formatAdress = adress.split("\n");
				// String[] formatOpeningHours =
				// formatOpeningHours(openingHours);

				// item.setOpeningTime(formatOpeningHours);

				item.setAdress(formatAdress);
				list.add(item);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private String readJSON(int path) {
		String jsonTxt = "";
		try {

			InputStream is = ctx.getResources().openRawResource(path);
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			String line;

			while ((line = br.readLine()) != null) {
				jsonTxt += line;
			}

			br.close();
			is.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonTxt;
	}

	// setter & getter

	public static void setAdress(String street, String city) {
		adress = street + ", " + city;
	}

	public static void setPrivatePreDrinkLocation(String place) {
		privatePreDrinkLocation = place;
	}

	public static void setLevel(int lev) {
		level = lev;
	}
	
	public static void setNoAddressForPreDrinkAtHomeIsSet(boolean bool){
		noAddressForPreDrinkAtHomeYet = bool;
	}
	
	public static void setTakeMeHome(boolean bool){
		takeMeHomeIsClicked = bool;
	}
	
	public static void setNewTour(boolean tour){
		newTour = tour;
	}

	// 0 for no preDrinks, 1 for private, 2 for public
	public static void setPreDrinks(int preDr) {
		preDrinks = preDr;
	}

	public static void setBars(boolean bar) {
		bars = bar;
	}

	public static void setClubs(boolean club) {
		clubs = club;
	}

	public static void setReminder(boolean rem) {
		reminder = rem;
	}
	
	public static void setTimerStarted(boolean timer){
		timerStarted = timer;
	}
	

	public static ArrayList<BarItem> getBarList() {
		return barList;
	}

	public static String getPrivatePreDrinkLocation() {
		return privatePreDrinkLocation;
	}

	public static int getBarsInDatabase() {
		barsInDatabase = barList.size();
		return barsInDatabase;
	}

	public static int getLevel() {
		return level;
	}

	public static int getPreDrinks() {
		return preDrinks;
	}

	public static boolean getBars() {
		return bars;
	}

	public static boolean getClubs() {
		return clubs;
	}

	public static boolean getReminder() {
		return reminder;
	}

	public static String getAdress() {
		return adress;
	}
	
	public static boolean getTakeMeHomeIsClicked(){
		return takeMeHomeIsClicked;
	}
	
	public static boolean getTimerStarted(){
		return timerStarted;
		
	}
	
	
	public static boolean getAddressForPreDrinkAtHomeIsSet(){
		return noAddressForPreDrinkAtHomeYet;
	}
	
	public static boolean isNewTour(){
		return newTour;
	}
	
	public static void setIsVisible(boolean visible) { 
        mIsVisible = visible; 
   }

   public static boolean getIsVisible() {
        return mIsVisible;
   }

}