package com.example.geiles_barroulette;

import android.graphics.Bitmap;

public class BarItem {
	
	private String name, phone, openinghours = "";
	private boolean isClub = false, isPreDrink = false;
	private String[] adress, //adress[0] == Street name, [1] == plz + city
	openingTime; //openingTime[0] == monday, [1] == tuesday,...

	private Bitmap image;
	
	public BarItem (String name){
		this.name = name;
	}
	
	
	public String getName(){
		return name;
	}
	
	public String[] getAdress(){
		return adress;
	}
	
	public String[] getOpeningTime(){
		return openingTime;
	}
	
	public Bitmap getImage(){
		return image;
	}
	
	public String getPhoneNumber(){
		return phone;
	}
	
	public String getOpeninghours(){
		return openinghours; 
	}
	
	
	public void setName(String name){
		this.name = name;
	}
	
	public boolean getIsClub(){
		return isClub;
	}
	
	public void setAdress(String[] adress){
		this.adress = adress;
	}
	
	public void setOpeningTime(String[] openingTime){
		this.openingTime = openingTime;
	}
	
	public void setImage(Bitmap image){
		this.image = image;
	}

	
	public void setPhoneNumber(String phone){
		this.phone = phone;
	}
	
	public void setOpeninghours(String hours){
		this.openinghours = hours;
	}
	
	public void setIsClub(boolean bool){
		isClub = bool;
	}


	public boolean getIsPreDrink() {
		return isPreDrink;
	}


	public void setPreDrink(boolean isPreDrink) {
		this.isPreDrink = isPreDrink;
	}
}